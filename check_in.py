import requests
from bs4 import BeautifulSoup
import re
import datetime
import os

headers = {'User-Agent': 'Mozilla/5.0'}
user = os.environ["USER"]
pwd = os.environ["PASSWORD"]
pause = os.environ["PAUSE"]
payload = {'data[User][email]':user,'data[User][password]':pwd}

session = requests.Session()

s=session.post("https://panel.sesametime.com/",headers=headers,data=payload)
v = session.get("https://panel.sesametime.com/admin/vacations/view")
soup = BeautifulSoup(v.text, 'html.parser')

holidays=[]
for i in soup.find_all("td", _title=re.compile(".+")):
    d = datetime.datetime.strptime(i.get("value"), "%Y-%m-%d")
    holidays.append(d.date())

today = datetime.date.today()


if today not in holidays:
    if pause:
        session.get("https://panel.sesametime.com/admin/checks/check_panel/7")
    else:
        session.get("https://panel.sesametime.com/admin/checks/check_panel/1")

    










